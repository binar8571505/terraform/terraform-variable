output "rng_result" {
    description = "Return random string result"
    value = random_string.rng.result
}

output "rng_length" {
    description = "Check random string length"
    value = random_string.rng.length
}