resource "google_compute_instance" "example_1" {
    name = "example-1"
    zone = var.instance_zone
    machine_type = "e2-micro"
    boot_disk {
        initialize_params {
            image = "ubuntu-os-cloud/ubuntu-2204-lts"
        }
    }
    network_interface {
        network = "default"
        access_config {
        }
    }
}