variable "instance_zone" {
    default = "asia-southeast2-a"
}

variable "random_length" {
    default = 8
    type = number
    description = "Variable untuk panjang random_string di random.tf"
}